#include "serialize.h"

#include "../parse/sql_parse.tab.h"
#include "../parse/structures.h"

protocol_predicate *serialize_predicate(predicate *predicate_info);

protocol_column *serialize_column(column *col) {
  protocol_column *data = g_object_new(TYPE_PROTOCOL_COLUMN, NULL);
  if (col->_name == NULL) {
    return data;
  }
  gchar *name = malloc(sizeof(gchar) * (strlen(col->_name) + 1));
  if (name == NULL) {
    return data;
  }
  strcpy(name, col->_name);
  name[strlen(col->_name)] = '\0';
  g_object_set(data, "name_", name, NULL);
  return data;
}

GPtrArray *serialize_columns(column *cur) {
  GPtrArray *columns_ptr = g_ptr_array_new();
  while (cur != NULL) {
    g_ptr_array_add(columns_ptr, serialize_column(cur));
    cur = cur->_next;
  }
  return columns_ptr;
}

protocol_literal *serialize_literal(literal *lit) {
  protocol_literal *data = g_object_new(TYPE_PROTOCOL_LITERAL, NULL);
  protocol_literal_union_storage *literalUnion =
      g_object_new(TYPE_PROTOCOL_LITERAL_UNION_STORAGE, NULL);
  switch (lit->_type) {
    case LITERAL_BOOLEAN:
      g_object_set(literalUnion, "boolean_", lit->_data._boolean, NULL);
      break;
    case LITERAL_INTEGER:
      g_object_set(literalUnion, "integer_", lit->_data._integer, NULL);
      break;
    case LITERAL_FLOAT:
      g_object_set(literalUnion, "float_", lit->_data._float, NULL);
      break;
    case LITERAL_STRING: {
      if (lit->_data._string == NULL) {
        break;
      }
      gchar *str = malloc(sizeof(gchar) * (strlen(lit->_data._string) + 1));
      if (str == NULL) {
        break;
      }
      strcpy(str, lit->_data._string);
      str[strlen(lit->_data._string)] = '\0';
      g_object_set(literalUnion, "string_", str, NULL);
      break;
    }
  }
  g_object_set(data, "data_", literalUnion, "type_", lit->_type, NULL);
  return data;
}

protocol_compare_right_part *serialize_compare_right_part(
    compare_right_part *part) {
  protocol_compare_right_part *data =
      g_object_new(TYPE_PROTOCOL_COMPARE_RIGHT_PART, NULL);
  protocol_compare_right_part_union *partUnion =
      g_object_new(TYPE_PROTOCOL_COMPARE_RIGHT_PART_UNION, NULL);
  switch (part->_type) {
    case COMPARE_COLUMN:
      g_object_set(partUnion, "column_", serialize_column(part->value._column),
                   NULL);
      break;
    case COMPARE_LITERAL:
      g_object_set(partUnion, "literal_",
                   serialize_literal(part->value._literal), NULL);
      break;
  }
  g_object_set(data, "type_", part->_type, "value_", partUnion, NULL);
  return data;
}

protocol_compare *serialize_compare(compare *compare_info) {
  protocol_compare *data = g_object_new(TYPE_PROTOCOL_COMPARE, NULL);
  g_object_set(data, "left_", serialize_column(compare_info->_left),
               "compare_type_", compare_info->_compare_type, "right_",
               serialize_compare_right_part(compare_info->_right), NULL);
  return data;
}

protocol_binary *serialize_binary(binary *binary_info) {
  protocol_binary *data = g_object_new(TYPE_PROTOCOL_BINARY, NULL);
  GPtrArray *left = g_ptr_array_new();
  g_ptr_array_add(left, serialize_predicate(binary_info->_left));
  GPtrArray *right = g_ptr_array_new();
  g_ptr_array_add(right, serialize_predicate(binary_info->_right));
  g_object_set(data, "left_", left, "right_", right, "type_",
               binary_info->_type, NULL);
  return data;
}

protocol_predicate *serialize_predicate(predicate *predicate_info) {
  protocol_predicate *data = g_object_new(TYPE_PROTOCOL_PREDICATE, NULL);
  protocol_predicate_union *predicateUnion =
      g_object_new(TYPE_PROTOCOL_PREDICATE_UNION, NULL);
  switch (predicate_info->_type) {
    case PREDICATE_COMPARE:
      g_object_set(predicateUnion, "compare_",
                   serialize_compare(predicate_info->content._compare), NULL);
      break;
    case PREDICATE_BINARY:
      g_object_set(predicateUnion, "binary_",
                   serialize_binary(predicate_info->content._binary), NULL);
      break;
    case PREDICATE_NEGATIVE: {
      GPtrArray *value = g_ptr_array_new();
      g_ptr_array_add(value,
                      serialize_predicate(predicate_info->content._negative));
      g_object_set(predicateUnion, "negative_", value, NULL);
      break;
    }
  }
  g_object_set(data, "content_", predicateUnion, "type_", predicate_info->_type,
               NULL);
  return data;
}

protocol_select_request *serialize_select(select_request *select) {
  protocol_select_request *data =
      g_object_new(TYPE_PROTOCOL_SELECT_REQUEST, NULL);
  g_object_set(data, "predicate_", g_ptr_array_new(), NULL);
  if (select->_predicate != NULL) {
    g_ptr_array_add(data->predicate_, serialize_predicate(select->_predicate));
  }
  g_object_set(data, "column_", serialize_columns(select->_column), NULL);
  data->__isset_column_ = 1;
  return data;
}

GPtrArray *serialize_columns_definition(columns_definition *definition) {
  GPtrArray *data = g_ptr_array_new();
  while (definition != NULL) {
    protocol_columns_definition *element =
        g_object_new(TYPE_PROTOCOL_COLUMNS_DEFINITION, NULL);
    gchar *name = malloc(sizeof(gchar) * (strlen(definition->_name) + 1));
    if (name != NULL) {
      strcpy(name, definition->_name);
      name[strlen(definition->_name)] = '\0';
    }
    g_object_set(element, "name_", name, "type_", definition->_type, NULL);
    g_ptr_array_add(data, element);
    definition = definition->_next;
  }
  return data;
}

GPtrArray *serialize_columns_update(columns_update *col_upd) {
  GPtrArray *data = g_ptr_array_new();
  while (col_upd != NULL) {
    protocol_columns_update *element =
        g_object_new(TYPE_PROTOCOL_COLUMNS_UPDATE, NULL);
    g_object_set(element, "column_", serialize_column(col_upd->_column),
                 "literal_", serialize_literal(col_upd->_literal), NULL);
    g_ptr_array_add(data, element);
    col_upd = col_upd->_next;
  }
  return data;
}

protocol_update_request *serialize_update(update_request *update) {
  protocol_update_request *data =
      g_object_new(TYPE_PROTOCOL_UPDATE_REQUEST, NULL);
  g_object_set(data, "predicate_", serialize_predicate(update->_predicate),
               "columns_", serialize_columns_update(update->_columns), NULL);
  return data;
}

GPtrArray *serialize_literals_list(literal_list *cur) {
  GPtrArray *data = g_ptr_array_new();
  while (cur != NULL) {
    g_ptr_array_add(data, serialize_literal(cur->_literal));
    cur = cur->_next;
  }
  return data;
}

protocol_insert_request *serialize_insert(insert_request *insert) {
  protocol_insert_request *data =
      g_object_new(TYPE_PROTOCOL_INSERT_REQUEST, NULL);
  g_object_set(data, "column_", serialize_columns(insert->_column), "list_",
               serialize_literals_list(insert->_list), NULL);
  return data;
}

protocol_request *serialize(request *rq) {
  protocol_request *data = g_object_new(TYPE_PROTOCOL_REQUEST, NULL);
  protocol_request_union *requestUnion =
      g_object_new(TYPE_PROTOCOL_REQUEST_UNION, NULL);
  switch (rq->_type) {
    case SELECT: {
      g_object_set(requestUnion, "select_", serialize_select(rq->_data._select),
                   NULL);
      break;
    }
    case CREATE:
      g_object_set(requestUnion, "create_",
                   serialize_columns_definition(rq->_data._create), NULL);
      break;
    case UPDATE:
      g_object_set(requestUnion, "update_", serialize_update(rq->_data._update),
                   NULL);
      break;
    case DROP:
      break;
    case INSERT:
      g_object_set(requestUnion, "insert_", serialize_insert(rq->_data._insert),
                   NULL);
      break;
    case DELETE:
      g_object_set(requestUnion, "delete_",
                   serialize_predicate(rq->_data._delete), NULL);
      break;
    default:
      return NULL;
  }
  g_object_set(data, "data_", requestUnion, "type_", rq->_type, NULL);
  if (rq->_name == NULL) {
    return data;
  }
  gchar *name = malloc(sizeof(gchar) * (strlen(rq->_name) + 1));
  if (name == NULL) {
    return data;
  }
  strcpy(name, rq->_name);
  name[strlen(rq->_name)] = '\0';
  g_object_set(data, "name_", name, NULL);
  return data;
}
