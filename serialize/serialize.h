#ifndef DALLVOROLLP3_SERIALIZE_H
#define DALLVOROLLP3_SERIALIZE_H

#include "../parse/structures.h"
#include "../protocols/gen-c_glib/protocols_types.h"

protocol_request *serialize(request *rq);

#endif  // DALLVOROLLP3_SERIALIZE_H
