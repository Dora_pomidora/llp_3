#include <glib.h>
#include <stdio.h>
#include <thrift/c_glib/protocol/thrift_binary_protocol_factory.h>
#include <thrift/c_glib/server/thrift_simple_server.h>
#include <thrift/c_glib/thrift.h>
#include <thrift/c_glib/transport/thrift_buffered_transport_factory.h>
#include <thrift/c_glib/transport/thrift_server_socket.h>

#include "database.h"
#include "database_dir/database/include/database.h"
#include "protocols/gen-c_glib/protocol_get_service.h"
#include "protocols/gen-c_glib/protocols_types.h"

G_BEGIN_DECLS

#define TYPE_PROTOCOL_GET_HANDLER (protocol_get_handler_get_type())
#define PROTOCOL_GET_HANDLER(obj)                               \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_PROTOCOL_GET_HANDLER, \
                              protocol_get_handler_impl))
#define PROTOCOL_GET_HANDLER_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_CAST((obj), TYPE_PROTOCOL_GET_HANDLER, protocol_get_handler_implClass)
#define IS_PROTOCOL_GET_HANDLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), TYPE_PROTOCOL_GET_HANDLER))
#define IS_PROTOCOL_GET_HANDLER_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE((obj), TYPE_PROTOCOL_GET_HANDLER))
#define PROTOCOL_GET_HANDLER_GET_CLASS(obj)                    \
  (G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_PROTOCOL_GET_HANDLER, \
                             protocol_get_handler_implClass))

struct _protocol_get_handler_impl {
  protocol_get_serviceHandler parent_instance;
};
typedef struct _protocol_get_handler_impl protocol_get_handler_impl;

struct _protocol_get_handler_implClass {
  protocol_get_serviceHandlerClass parent_class;
};
typedef struct _protocol_get_handler_implClass protocol_get_handler_implClass;

GType protocol_get_handler_get_type(void);

G_END_DECLS

G_DEFINE_TYPE(protocol_get_handler_impl, protocol_get_handler,
              TYPE_PROTOCOL_GET_SERVICE_HANDLER)

ThriftServer *server;

static int cnt = 0;

static void protocol_get_handler_init(protocol_get_handler_impl *self) {}

static gboolean protocol_get_handler_execute(protocol_get_serviceIf *i,
                                             protocol_response **response,
                                             const protocol_request *rq,
                                             GError **error) {
  THRIFT_UNUSED_VAR(i);
  THRIFT_UNUSED_VAR(error);
  if (rq->type_ == -1) {
    thrift_server_stop(server);
    return 0;
  }
  process_request(rq, *response);

  return 1;
}

static void protocol_get_handler_finalize(GObject *obj) {
  protocol_get_handler_impl *self = PROTOCOL_GET_HANDLER(obj);
  G_OBJECT_CLASS(protocol_get_handler_parent_class)->finalize(obj);
}

static void protocol_get_handler_class_init(
    protocol_get_handler_implClass *self) {
  GObjectClass *obj = G_OBJECT_CLASS(self);
  protocol_get_serviceHandlerClass *exchange =
      PROTOCOL_GET_SERVICE_HANDLER_CLASS(self);
  obj->finalize = protocol_get_handler_finalize;
  exchange->execute = protocol_get_handler_execute;
}

int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "usage: ./server <filename> <port>");
    return -1;
  }

  int port = atoi(argv[2]);

  protocol_get_handler_impl *handler =
      g_object_new(TYPE_PROTOCOL_GET_HANDLER, NULL);
  protocol_get_serviceProcessor *processor = g_object_new(
      TYPE_PROTOCOL_GET_SERVICE_PROCESSOR, "handler", handler, NULL);

  ThriftServerTransport *transport =
      g_object_new(THRIFT_TYPE_SERVER_SOCKET, "port", port, NULL);
  ThriftTransportFactory *factory =
      g_object_new(THRIFT_TYPE_BUFFERED_TRANSPORT_FACTORY, NULL);
  ThriftProtocolFactory *protocol =
      g_object_new(THRIFT_TYPE_BINARY_PROTOCOL_FACTORY, NULL);
  server = g_object_new(
      THRIFT_TYPE_SIMPLE_SERVER, "processor", processor, "server_transport",
      transport, "input_transport_factory", factory, "output_transport_factory",
      factory, "input_protocol_factory", protocol, "output_protocol_factory",
      protocol, NULL);

  GError *error = NULL;

  open_database(argv[1]);

  thrift_server_serve(server, &error);

  close_database();

  g_object_unref(server);
  g_object_unref(protocol);
  g_object_unref(factory);
  g_object_unref(transport);
  g_object_unref(processor);
  g_object_unref(handler);
  return 0;
}
