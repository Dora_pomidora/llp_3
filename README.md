# Сборка

Для работы понадобятся:
* thrift
* gcc
* cmake
* thrift-c_glib
* bison
* flex
* glibc
* gobject

```
thrift -o protocols --gen c_glib protocols.thrift
cd parse
make sql_parse
cd ..
mkdir build && cd build
cmake ..
make
```

# Запуск 

Из корня проекта следует запустить:

`./build/DallvoroLLP3_SERVER <path-to-file> <port>`

`./build/DallvoroLLP3_CLIENT <ip=127.0.0.1 default> <port>`

