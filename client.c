#include <glib.h>
#include <stdio.h>
#include <thrift/c_glib/protocol/thrift_binary_protocol.h>
#include <thrift/c_glib/transport/thrift_buffered_transport.h>
#include <thrift/c_glib/transport/thrift_socket.h>

#include "database_dir/database_utils/include/database_utils.h"
#include "parse/parse_request.h"
#include "protocols/gen-c_glib/protocol_get_service.h"
#include "serialize/serialize.h"

#define BUFFER_LENGTH (1 << 16)

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "usage: ./client <hostname> <port>.\n");
    return -1;
  }
  int port = atoi(argv[2]);

  ThriftSocket *socket =
      g_object_new(THRIFT_TYPE_SOCKET, "hostname", argv[1], "port", port, NULL);
  ThriftTransport *transport =
      g_object_new(THRIFT_TYPE_BUFFERED_TRANSPORT, "transport", socket, NULL);
  ThriftProtocol *protocol =
      g_object_new(THRIFT_TYPE_BINARY_PROTOCOL, "transport", transport, NULL);

  GError *error = NULL;
  thrift_transport_open(transport, &error);
  if (error != NULL) {
    g_object_unref(socket);
    g_object_unref(transport);
    g_object_unref(protocol);
    return -1;
  }

  protocol_get_serviceIf *service =
      g_object_new(TYPE_PROTOCOL_GET_SERVICE_CLIENT, "input_protocol", protocol,
                   "output_protocol", protocol, NULL);

  char buffer[BUFFER_LENGTH];

  for (;;) {
    printf(" > REPL: ");
    fgets(buffer, BUFFER_LENGTH, stdin);
    struct result_parse data = parse(buffer);
    if (data.exit_code == 1) {
      protocol_request *req =
          g_object_new(TYPE_PROTOCOL_REQUEST, "type_", -1, NULL);
      protocol_response *response = g_object_new(TYPE_PROTOCOL_RESPONSE, NULL);
      gboolean result =
          protocol_get_service_if_execute(service, &response, req, &error);
      g_object_unref(response);
      break;
    }
    protocol_request *req = serialize(data.data);
    deleteRequest(data.data);
    protocol_response *response = g_object_new(TYPE_PROTOCOL_RESPONSE, NULL);
    gboolean result =
        protocol_get_service_if_execute(service, &response, req, &error);
    printf("%d %s\n", response->status_, response->message_);
    if (response->__isset_result_) {
      for (guint i = 0; i < response->result_->column_name_->len; ++i) {
        printf("%s",
               ((protocol_column *)response->result_->column_name_->pdata[i])
                   ->name_);
        if (i + 1 < response->result_->column_name_->len) {
          printf(" ");
        }
      }
      printf("\n");
      for (guint i = 0; i < response->result_->data_->len; ++i) {
        protocol_row_in_table *cur =
            ((protocol_row_in_table *)response->result_->data_->pdata[i]);
        for (guint j = 0; j < cur->values_->len; ++j) {
          protocol_literal *now = (protocol_literal *)cur->values_->pdata[j];
          switch (now->type_) {
            case COLUMN_BOOL:
              printf(now->data_->boolean_ ? "true" : "false");
              break;
            case COLUMN_INT32:
              printf("%d", now->data_->integer_);
              break;
            case COLUMN_DOUBLE:
              printf("%f", now->data_->float_);
              break;
            case COLUMN_STRING:
              printf("%s", now->data_->string_);
              break;
          }
          if (j + 1 < cur->values_->len) {
            printf(" ");
          }
        }
        printf("\n");
      }
    }
    g_object_unref(response);
  }

  thrift_transport_close(transport, &error);

  int return_value = error != NULL;

  g_clear_error(&error);

  g_object_unref(socket);
  g_object_unref(transport);
  g_object_unref(protocol);
  g_object_unref(service);

  return return_value;
}
