#ifndef DATABASELLP_FILE_INTERFACE_H
#define DATABASELLP_FILE_INTERFACE_H

#include <sys/types.h>

#define CHUNK_SIZE 32

int file_create(const char *filename);

int file_open(const char *filename);

int file_close();

int file_delete(const char *filename);

int file_write(const void *buf, size_t size);

ssize_t file_read(void *buf, size_t size);

int write_move(off_t pos);

int read_move(off_t pos);

off_t file_size();

int write_in_pos(off_t pos, const void *buf, size_t size);

int read_from_pos(off_t pos, void *buf, size_t size);

#endif  // DATABASELLP_FILE_INTERFACE_H
