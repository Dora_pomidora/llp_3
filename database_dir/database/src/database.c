#include "database.h"

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../../../parse/structures.h"
#include "file_interface.h"
#include "header_utils.h"
#include "logger.h"

database_file_header* header = NULL;
const char* table_name = NULL;
int exists_db = 0;

int create_database(const char* filename, const GPtrArray* columns) {
  if (header != NULL) {
    return 400;
  }
  if (strcmp(filename, table_name)) {
    return 400;
  }
  int err_code = 200;
  if ((header = create_header(filename, columns)) == NULL) {
    err_code = 400;
    goto end;
  }
  if (write_file_header(header) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing file header of init database_dir.");
    err_code = 400;
    goto end;
  }
end:
  return err_code;
}

int open_database(const char* filename) {
  table_name = filename;
  if (access(filename, F_OK) != 0) {
    file_create(filename);
    return 0;
  }
  if (file_open(filename) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on open database_dir with name %s.", filename);
    return -1;
  }
  if ((header = read_file_header()) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading a file-header.");
    return -1;
  }
  exists_db = 1;
  return 0;
}

int close_database() {
  int err_code = 0;
  if (write_file_header(header) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing a file-header.");
    err_code = -1;
  }
  if (file_close() == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error onclosing a database_dir.");
    err_code = -1;
  }
  free(header);
  return err_code;
}

int delete_database(const char* filename) {
  if (access(filename, F_OK) != 0) {
    return -2;
  }
  if (file_delete(filename) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on deleting a database_dir.");
    return -1;
  }
  return 0;
}

void process_create(const GPtrArray* columns, const gchar* name,
                    protocol_response* response) {
  g_object_set(response, "status_", create_database(name, columns), NULL);
  if (response->status_ == 200) {
    g_object_set(response, "message_", "CREATE: OK.", NULL);
  } else {
    g_object_set(response, "message_", "Error", NULL);
  }
}

typedef struct {
  off_t pos;
  size_t size;
} pos_to_ins;

pos_to_ins* find_pos(size_t size) {
  pos_to_ins* result = malloc(sizeof(pos_to_ins));
  database_row* data = NULL;
  int err_code = 0;
  if (result == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a result of find_pos.");
    return NULL;
  }
  if (header->next_free_block == 0) {
    result->size = 0;
    if ((result->pos = file_size()) == -1) {
      err_code = -1;
    }
    goto end;
  }
  if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
    err_code = -1;
    goto end;
  }
  if (read_from_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
    goto end;
  }
  if (data->size < size) {
    result->size = 0;
    if ((result->pos = file_size()) == -1) {
      err_code = -1;
    }
    goto end;
  }
  if (header->next_free_block == header->prev_free_block) {
    result->pos = header->next_free_block;
    result->size = data->size;
    header->prev_free_block = 0;
    header->next_free_block = 0;
    goto end;
  }
  result->pos = header->next_free_block;
  result->size = data->size;
  header->next_free_block = data->next;
  if (read_from_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
    goto end;
  }
  data->prev = 0;
  if (write_in_pos(header->next_free_block, data, ROW_PADDING_SIZE) == -1) {
    err_code = -1;
  }
end:
  if (data != NULL) {
    free(data);
  }
  if (err_code == -1) {
    free(result);
    result = NULL;
  }
  return result;
}

void left_header_link(off_t pos, database_row* data) {
  off_t data_offset = (off_t)ROW_PADDING_SIZE;
  off_t header_offset = (off_t)FILE_HEADER_PADDING_SIZE;
  database_column* cur_column =
      (database_column*)((char*)header + header_offset);
  char* cur_data = (char*)data + data_offset;
  header->next_data_block = pos;
  data->prev = 0;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    database_element* cur_element = (database_element*)cur_data;
    cur_column->next = pos + data_offset;
    cur_element->prev = header_offset;
    header_offset += COLUMN_PADDING_SIZE;
    size_t offset_diff = ELEMENT_PADDING_SIZE + padding(cur_element->size);
    data_offset += (off_t)offset_diff;
    cur_data += offset_diff;
  }
}

void right_header_link(off_t pos, database_row* data) {
  off_t data_offset = (off_t)ROW_PADDING_SIZE;
  off_t header_offset = (off_t)FILE_HEADER_PADDING_SIZE;
  database_column* cur_column =
      (database_column*)((char*)header + header_offset);
  char* cur_data = (char*)data + data_offset;
  header->prev_data_block = pos;
  data->next = 0;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    database_element* cur_element = (database_element*)cur_data;
    cur_column->prev = pos + data_offset;
    cur_element->next = header_offset;
    header_offset += COLUMN_PADDING_SIZE;
    size_t offset = ELEMENT_PADDING_SIZE + padding(cur_element->size);
    data_offset += (off_t)offset;
    cur_data += offset;
  }
}

void link_rows(off_t left_pos, database_row* left, off_t right_pos,
               database_row* right) {
  off_t left_offset = (off_t)ROW_PADDING_SIZE;
  off_t right_offset = (off_t)ROW_PADDING_SIZE;
  left->next = right_pos;
  right->prev = left_pos;
  char* cur_left = (char*)left + left_offset;
  char* cur_right = (char*)right + right_offset;
  for (size_t i = 0; i < header->columns_cnt; ++i) {
    database_element* left_element = (database_element*)cur_left;
    database_element* right_element = (database_element*)cur_right;
    left_element->next = right_pos + right_offset;
    right_element->prev = left_pos + left_offset;
    size_t cur_left_offset = ELEMENT_PADDING_SIZE + padding(left_element->size);
    size_t cur_right_offset =
        ELEMENT_PADDING_SIZE + padding(right_element->size);
    left_offset += (off_t)cur_left_offset;
    right_offset += (off_t)cur_right_offset;
    cur_left += cur_left_offset;
    cur_right += cur_right_offset;
  }
}

void process_insert(protocol_insert_request* req, const gchar* name,
                    protocol_response* response) {
  if (req->column_->len != req->list_->len) {
    if (response != NULL)
      g_object_set(response, "message_", "Error: columns cnt != values cnt.",
                   "status_", 400, NULL);
    return;
  }
  if (header->columns_cnt != req->column_->len) {
    if (response != NULL)
      g_object_set(response, "message_",
                   "Error: columns cnt != table columns cnt.", "status_", 400,
                   NULL);
    return;
  }
  database_row* data = create_row(header, req);
  pos_to_ins* res = find_pos(data->size);
  off_t pos = 0;
  if (res == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on finding position to insert a row.");
    pos = -1;
    goto end;
  }
  pos = res->pos;
  if (res->size != 0) {
    data->padding = res->size - data->size;
  }
  free(res);
  off_t prev = header->prev_data_block;
  right_header_link(pos, data);
  if (prev == 0) {
    left_header_link(pos, data);
  } else {
    database_row* prev_data;
    if ((prev_data = read_row(prev)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading an previous row.");
      pos = -1;
      goto end;
    }
    link_rows(prev, prev_data, pos, data);
    if (write_row(prev, prev_data) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on rewriting previous row to database.");
      free(prev_data);
      pos = -1;
      goto end;
    }
    free(prev_data);
  }
  if (write_row(pos, data) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing new row to database.");
    pos = -1;
    goto end;
  }
end:
  if (data != NULL) {
    free(data);
  }
  if (response != NULL) {
    if (pos == -1) {
      g_object_set(response, "message_", "Error", "status_", 400, NULL);
    } else {
      g_object_set(response, "message_", "INSERT: OK.", "status_", 200, NULL);
    }
  }
}

void fill_start_positions(off_t* positions, database_file_header* header,
                          const GPtrArray* columns) {
  for (size_t i = 0; i < columns->len; ++i) {
    database_column* cur_column =
        (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
    for (size_t j = 0; j < header->columns_cnt; ++j, ++cur_column) {
      if (!strcmp(cur_column->name,
                  ((const protocol_column*)columns->pdata[i])->name_)) {
        *(positions + i) = cur_column->next;
        break;
      }
    }
  }
}

int cmp_string(COMPARE_TYPE compare_type, const char* s1, const char* s2) {
  int cmp_value = strcmp(s1, s2);
  switch (compare_type) {
    case DATABASE_LESS:
      return cmp_value < 0;
    case DATABASE_LESS_EQ:
      return cmp_value <= 0;
    case DATABASE_EQ:
      return cmp_value == 0;
    case DATABASE_NOT_EQ:
      return cmp_value != 0;
    case DATABASE_GREATER_EQ:
      return cmp_value >= 0;
    case DATABASE_GREATER:
      return cmp_value > 0;
    case DATABASE_CONTAINS:
      return strstr(s1, s2) != NULL;
    default:
      return 1;
  }
}

#define CMP_TEMPLATE(_type)                                                    \
  int cmp_##_type(COMPARE_TYPE compare_type, const char* v1, const char* v2) { \
    _type lhs = *(const _type*)v1;                                             \
    _type rhs = *(const _type*)v2;                                             \
    switch (compare_type) {                                                    \
      case DATABASE_LESS:                                                      \
        return lhs < rhs;                                                      \
      case DATABASE_LESS_EQ:                                                   \
        return lhs <= rhs;                                                     \
      case DATABASE_EQ:                                                        \
        return lhs == rhs;                                                     \
      case DATABASE_NOT_EQ:                                                    \
        return lhs != rhs;                                                     \
      case DATABASE_GREATER_EQ:                                                \
        return lhs >= rhs;                                                     \
      case DATABASE_GREATER:                                                   \
        return lhs > rhs;                                                      \
      default:                                                                 \
        return 1;                                                              \
    }                                                                          \
  }

CMP_TEMPLATE(double)
CMP_TEMPLATE(int32_t)
CMP_TEMPLATE(char)

int compare_literal(int type, off_t* positions, database_element* element,
                    const protocol_compare_right_part_union* data) {
  const char* start = ((const char*)element) + ELEMENT_PADDING_SIZE;
  switch (data->literal_->type_) {
    case LITERAL_BOOLEAN: {
      char __bool = data->literal_->data_->boolean_;
      return cmp_char(type, start, &__bool);
    }
    case LITERAL_INTEGER:
      return cmp_int32_t(type, start, (char*)&data->literal_->data_->integer_);
    case LITERAL_FLOAT:
      return cmp_double(type, start, (char*)&data->literal_->data_->float_);
    case LITERAL_STRING:
      return cmp_string(type, start, data->literal_->data_->string_);
  }
  return 0;
}

int compare_column(int type, off_t* positions, database_element* element,
                   const protocol_compare_right_part_union* data,
                   const GPtrArray* columns) {
  const char* start = ((const char*)element) + ELEMENT_PADDING_SIZE;
  guint position = -1;
  for (guint i = 0; i < columns->len; ++i) {
    if (!strcmp(((const protocol_column*)columns->pdata[i])->name_,
                data->column_->name_)) {
      position = i;
      break;
    }
  }
  if (position == -1) {
    return 0;
  }
  database_element* right = read_element(positions[position], 1);
  if (right == NULL) {
    return 0;
  }
  const char* right_start = ((const char*)right) + ELEMENT_PADDING_SIZE;
  int rtrn = 0;
  switch (element->type) {
    case LITERAL_BOOLEAN:
      rtrn = cmp_char(type, start, right_start);
      break;
    case LITERAL_INTEGER:
      rtrn = cmp_int32_t(type, start, right_start);
      break;
    case LITERAL_FLOAT:
      rtrn = cmp_double(type, start, right_start);
      break;
    case LITERAL_STRING:
      rtrn = cmp_string(type, start, right_start);
      break;
  }
  free(right);
  return rtrn;
}

int validate_row(off_t* positions, const GPtrArray* columns,
                 const protocol_predicate* predicate_info) {
  switch (predicate_info->type_) {
    case PREDICATE_BINARY: {
      int left_val = 0;
      if (predicate_info->content_->binary_->left_->len != 0) {
        left_val = validate_row(positions, columns,
                                (const protocol_predicate*)predicate_info
                                    ->content_->binary_->left_->pdata[0]);
      }
      int right_val = 0;
      if (predicate_info->content_->binary_->right_->len != 0) {
        right_val = validate_row(positions, columns,
                                 (const protocol_predicate*)predicate_info
                                     ->content_->binary_->right_->pdata[0]);
      }
      switch (predicate_info->content_->binary_->type_) {
        case 281:
          return left_val | right_val;
        case 282:
          return left_val & right_val;
      }
      return 0;
    }
    case PREDICATE_NEGATIVE:
      if (predicate_info->content_->negative_->len == 0) {
        return 0;
      }
      return !validate_row(positions, columns,
                           (const protocol_predicate*)
                               predicate_info->content_->negative_->pdata[0]);
    case PREDICATE_COMPARE: {
      size_t position = -1;
      for (size_t i = 0; i < columns->len; ++i) {
        if (!strcmp(((protocol_column*)columns->pdata[i])->name_,
                    predicate_info->content_->compare_->left_->name_)) {
          position = i;
          break;
        }
      }
      if (position == -1) {
        return 0;
      }
      int type;
      switch (predicate_info->content_->compare_->compare_type_) {
        case 286:
          type = DATABASE_EQ;
          break;
        case 287:
          type = DATABASE_NOT_EQ;
          break;
        case 288:
          type = DATABASE_LESS;
          break;
        case 289:
          type = DATABASE_LESS_EQ;
          break;
        case 290:
          type = DATABASE_GREATER;
          break;
        case 291:
          type = DATABASE_GREATER_EQ;
          break;
        case 285:
          type = DATABASE_CONTAINS;
          break;
      }
      database_element* element = read_element(positions[position], 1);
      if (element == NULL) {
        return 0;
      }
      int rtrn = 0;
      switch (predicate_info->content_->compare_->right_->type_) {
        case COMPARE_LITERAL:
          rtrn = compare_literal(
              type, positions, element,
              predicate_info->content_->compare_->right_->value_);
          break;
        case COMPARE_COLUMN:
          rtrn = compare_column(
              type, positions, element,
              predicate_info->content_->compare_->right_->value_, columns);
          break;
      }
      free(element);
      return rtrn;
    }
  }
  return 0;
}

void fill_columns_name(protocol_response* response, const GPtrArray* columns) {
  for (guint i = 0; i < columns->len; ++i) {
    g_ptr_array_add(response->result_->column_name_,
                    (protocol_column*)columns->pdata[i]);
  }
}

protocol_row_in_table* data_to_row(off_t* positions, const GPtrArray* columns) {
  protocol_row_in_table* result = g_object_new(
      TYPE_PROTOCOL_ROW_IN_TABLE, "values_", g_ptr_array_new(), NULL);
  for (guint i = 0; i < columns->len; ++i) {
    database_element* element = read_element(positions[i], 1);
    if (element == NULL) {
      return NULL;
    }
    char* start = ((char*)element) + ELEMENT_PADDING_SIZE;
    protocol_literal* lit =
        g_object_new(TYPE_PROTOCOL_LITERAL, "type_", element->type, NULL);
    protocol_literal_union_storage* un =
        g_object_new(TYPE_PROTOCOL_LITERAL_UNION_STORAGE, NULL);
    switch (element->type) {
      case COLUMN_BOOL:
        g_object_set(un, "boolean_", *start, NULL);
        break;
      case COLUMN_INT32:
        g_object_set(un, "integer_", *((int32_t*)start), NULL);
        break;
      case COLUMN_DOUBLE:
        g_object_set(un, "float_", *((double*)start), NULL);
        break;
      case COLUMN_STRING:
        g_object_set(un, "string_", strcpy(malloc(strlen(start) + 1), start),
                     NULL);
        break;
    }
    g_object_set(lit, "data_", un, NULL);
    g_ptr_array_add(result->values_, lit);
    free(element);
  }
  return result;
}

void process_select(const protocol_select_request* req, const gchar* name,
                    protocol_response* response) {
  g_object_set(response, "result_",
               g_object_new(TYPE_PROTOCOL_TABLE_RESULT, NULL), "status_", 200,
               NULL);
  g_object_set(response->result_, "data_", g_ptr_array_new(), "column_name_",
               g_ptr_array_new(), NULL);
  off_t* positions;
  if ((positions = malloc(sizeof(off_t) * req->column_->len)) == NULL) {
    g_object_set(response, "status_", 400, "message_",
                 "Error on allocating memory for a positions in select.", NULL);
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a positions in select.");
    return;
  }
  fill_columns_name(response, req->column_);
  fill_start_positions(positions, header, req->column_);
  off_t cur_row = header->next_data_block;
  int count = 0;
  while (cur_row != 0) {
    int code =
        (req->predicate_->len == 0
             ? 1
             : validate_row(
                   positions, req->column_,
                   (const protocol_predicate*)req->predicate_->pdata[0]));
    if (code == 1) {
      ++count;
      g_ptr_array_add(response->result_->data_,
                      data_to_row(positions, req->column_));
    }
    for (size_t i = 0; i < req->column_->len; ++i) {
      database_element* data;
      if ((data = read_element(*(positions + i), 0)) == NULL) {
        g_object_set(response, "status_", 400, "message_",
                     "Error on reading meta-data of an element.", NULL);
        logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
               "Error on reading meta-data of an element.");
        goto end;
      }
      *(positions + i) = data->next;
      free(data);
    }
    database_row* next_row;
    if ((next_row = malloc(ROW_PADDING_SIZE)) == NULL) {
      g_object_set(response, "status_", 400, "message_",
                   "Error on allocating memory for meta-data of a row.", NULL);
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      goto end;
    }
    if (read_from_pos(cur_row, next_row, ROW_PADDING_SIZE) == -1) {
      g_object_set(response, "status_", 400, "message_",
                   "Error on reading meta-data of a row.", NULL);
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(next_row);
      goto end;
    }
    cur_row = next_row->next;
    free(next_row);
  }

end:
  if (response->status_ == 200) {
    char message[20];
    sprintf(message, "SELECT: %d", count);
    g_object_set(response, "message_", message, NULL);
  }
  free(positions);
}

int between_header(database_row* data) {
  if (data->prev != 0 || data->next != 0) {
    return -2;
  }
  header->prev_data_block = 0;
  header->next_data_block = 0;
  database_column* cur_column =
      (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    cur_column->prev = 0;
    cur_column->next = 0;
  }
  return 0;
}

int between_rows(database_row* data) {
  if (data->prev == 0 || data->next == 0) {
    return -2;
  }
  int err_code = 0;
  database_row* prev = NULL;
  database_row* next = NULL;
  if ((prev = read_row(data->prev)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading previous row of deleting part.");
    err_code = -1;
    goto end;
  }
  if ((next = read_row(data->next)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading next row of deleting part.");
    err_code = -1;
    goto end;
  }
  link_rows(data->prev, prev, data->next, next);
  if ((err_code = write_row(data->prev, prev)) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing previous row of deleting part.");
    goto end;
  }
  if ((err_code = write_row(data->next, next)) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing next row of deleting part.");
    goto end;
  }
end:
  if (prev != NULL) {
    free(prev);
  }
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int previous_header(database_row* data) {
  if (data->prev != 0) {
    return -2;
  }
  int err_code = 0;
  database_row* next = NULL;
  if ((next = read_row(data->next)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading next row of deleting part.");
    err_code = -1;
    goto end;
  }
  left_header_link(data->next, next);
  if ((err_code = write_row(data->next, next)) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing next row of deleting part.");
    goto end;
  }

end:
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int next_header(database_row* data) {
  if (data->next != 0) {
    return -2;
  }
  int err_code = 0;
  database_row* prev = NULL;
  if ((prev = read_row(data->prev)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading previous row of deleting part.");
    err_code = -1;
    goto end;
  }
  right_header_link(data->prev, prev);
  if ((err_code = write_row(data->prev, prev)) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing previous row of deleting part.");
    goto end;
  }

end:
  if (prev != NULL) {
    free(prev);
  }
  return err_code;
}

off_t* find_free(size_t size) {
  off_t prev = 0;
  off_t current = header->next_free_block;
  while (current != 0) {
    database_row* data;
    if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of free block.");
      return NULL;
    }
    if (read_from_pos(current, data, ROW_PADDING_SIZE) == -1) {
      free(data);
      return NULL;
    }
    if (data->size <= size) {
      free(data);
      break;
    }
    prev = current;
    current = data->next;
  }
  off_t* result;
  if ((result = malloc(2 * sizeof(off_t))) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a result of free blocks.");
    return NULL;
  }
  *result = prev;
  *(result + 1) = current;
  return result;
}

int insert_free(const off_t* positions, off_t pos, database_row* data) {
  if (*positions == 0 && *(positions + 1) == 0) {
    header->next_free_block = pos;
    header->prev_free_block = pos;
    data->prev = 0;
    data->next = 0;
    return 0;
  }
  if (*positions == 0) {
    database_row* next;
    if ((next = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for a meta-data of row.");
      return -1;
    }
    if (read_from_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(next);
      return -1;
    }
    data->prev = 0;
    data->next = *(positions + 1);
    next->prev = pos;
    header->next_free_block = pos;
    if (write_in_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on writing meta-data of a row.");
      free(next);
      return -1;
    }
    free(next);
    return 0;
  }
  if (*(positions + 1) == 0) {
    database_row* prev;
    if ((prev = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for a meta-data of row.");
      return -1;
    }
    if (read_from_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      free(prev);
      return -1;
    }
    data->prev = *positions;
    data->next = 0;
    header->prev_free_block = pos;
    prev->next = pos;
    if (write_in_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on writing meta-data of a row.");
      free(prev);
      return -1;
    }
    free(prev);
    return 0;
  }
  database_row* prev = NULL;
  database_row* next = NULL;
  int err_code = 0;
  if ((prev = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a meta-data of row.");
    err_code = -1;
    goto end;
  }
  if ((next = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a meta-data of row.");
    err_code = -1;
    goto end;
  }
  if (read_from_pos(*positions, prev, ROW_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a row.");
    err_code = -1;
    goto end;
  }
  if (read_from_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a row.");
    err_code = -1;
    goto end;
  }
  next->prev = pos;
  prev->next = pos;
  data->prev = *positions;
  data->next = *(positions + 1);
  if (write_in_pos(*(positions + 1), prev, ROW_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing meta-data of a row.");
    err_code = -1;
    goto end;
  }
  if (write_in_pos(*(positions + 1), next, ROW_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing meta-data of a row.");
    err_code = -1;
    goto end;
  }

end:
  if (prev != NULL) {
    free(prev);
  }
  if (next != NULL) {
    free(next);
  }
  return err_code;
}

int erase_database_index(off_t pos) {
  database_row* data;
  if ((data = read_row(pos)) == NULL) {
    return -1;
  }
  int err_code = between_header(data);
  if (err_code == 0) {
    goto end;
  }
  err_code = previous_header(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }
  err_code = next_header(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }
  err_code = between_rows(data);
  if (err_code == -1 || err_code == 0) {
    goto end;
  }

end:
  if (err_code == 0) {
    data->size += data->padding;
    data->padding = 0;
    off_t* free_positions = find_free(data->size);
    if ((err_code = insert_free(free_positions, pos, data)) == -1) {
      free(free_positions);
      goto rtn;
    }
    if ((err_code = write_in_pos(pos, data, ROW_PADDING_SIZE)) == -1) {
      free(free_positions);
      goto rtn;
    }
    free(free_positions);
  }
rtn:
  free(data);
  return err_code;
}

int next_rows(off_t* positions, size_t count) {
  int err_code = 0;
  for (size_t i = 0; i < count; ++i, ++positions) {
    database_element* data;
    if ((data = read_element(*positions, 0)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading a meta-data of an element.");
      err_code = -1;
      goto end;
    }
    *(positions + i) = data->next;
    free(data);
  }
end:
  return err_code;
}

int find_name(protocol_predicate* predicate_info, char* name) {
  if (predicate_info == NULL) {
    return 0;
  }
  int value = 0;
  switch (predicate_info->type_) {
    case PREDICATE_NEGATIVE:
      return find_name(
          (protocol_predicate*)predicate_info->content_->negative_->pdata[0],
          name);
    case PREDICATE_COMPARE:
      if (!strcmp(predicate_info->content_->compare_->left_->name_, name)) {
        return 1;
      }
      if (predicate_info->content_->compare_->compare_type_ == COMPARE_COLUMN &&
          !strcmp(predicate_info->content_->compare_->right_->value_->column_
                      ->name_,
                  name)) {
        return 1;
      }
      return 0;
    case PREDICATE_BINARY:
      return find_name((protocol_predicate*)
                           predicate_info->content_->binary_->left_->pdata[0],
                       name) |
             find_name((protocol_predicate*)
                           predicate_info->content_->binary_->right_->pdata[0],
                       name);
  }
  return 0;
}

void process_delete(protocol_predicate* predicate_info,
                    protocol_response* response) {
  GPtrArray* columns = g_ptr_array_new();
  for (size_t i = 0; i < header->columns_cnt; ++i) {
    database_column* cur =
        ((database_column*)((char*)header + FILE_HEADER_PADDING_SIZE)) + i;
    if (find_name(predicate_info, cur->name)) {
      g_ptr_array_add(columns, g_object_new(TYPE_PROTOCOL_COLUMN, "name_",
                                            cur->name, NULL));
    }
  }
  off_t* positions;
  if ((positions = malloc(sizeof(off_t) * columns->len)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a positions in select.");
    g_object_set(response, "status_", 400, "message_",
                 "Error on allocating memory", NULL);
    return;
  }
  size_t size = 0;
  for (size_t j = 0; j < columns->len; ++j) {
    for (size_t i = 0; i < header->columns_cnt; ++i) {
      database_column* cur =
          (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE) + i;
      if (!strcmp(((protocol_column*)columns->pdata[j])->name_, cur->name)) {
        positions[size++] = cur->next;
        break;
      }
    }
  }
  int err_code = 0;
  int count = 0;
  off_t cur_row = header->next_data_block;
  while (cur_row != 0) {
    int code = predicate_info == NULL
                   ? 1
                   : validate_row(positions, columns, predicate_info);
    database_row* next_row;
    if ((next_row = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on allocating memory", NULL);
      err_code = -1;
      goto end;
    }
    if ((err_code = read_from_pos(cur_row, next_row, ROW_PADDING_SIZE)) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on reading data", NULL);
      free(next_row);
      goto end;
    }
    off_t next_position = next_row->next;
    free(next_row);
    if (next_rows(positions, size) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on get next positions of elements.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on reading data", NULL);
      err_code = -1;
      goto end;
    }
    if (code == 1) {
      if (erase_database_index(cur_row) == -1) {
        err_code = -1;
        g_object_set(response, "status_", 400, "message_",
                     "Error on erasing data", NULL);
        goto end;
      } else {
        ++count;
      }
    }
    cur_row = next_position;
  }
end:
  if (err_code != -1) {
    char message[20];
    sprintf(message, "ERASE: %d.", count);
    g_object_set(response, "status_", 200, "message_", message, NULL);
  }
  for (guint i = 0; i < columns->len; ++i) {
    g_object_unref(columns->pdata[i]);
  }
  g_ptr_array_unref(columns);
  free(positions);
}

void process_update(GPtrArray* columns_update,
                    protocol_predicate* predicate_info,
                    protocol_response* response) {
  GPtrArray* columns = g_ptr_array_new();
  for (size_t i = 0; i < header->columns_cnt; ++i) {
    database_column* cur =
        ((database_column*)((char*)header + FILE_HEADER_PADDING_SIZE)) + i;
    if (find_name(predicate_info, cur->name)) {
      g_ptr_array_add(columns, g_object_new(TYPE_PROTOCOL_COLUMN, "name_",
                                            cur->name, NULL));
    }
  }
  off_t* positions;
  if ((positions = malloc(sizeof(off_t) * columns->len)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a positions in select.");
    g_object_unref(response);
    g_object_set(response, "status_", 400, "message_",
                 "Error on allocating memory", NULL);
    return;
  }
  size_t size = 0;
  for (size_t j = 0; j < columns->len; ++j) {
    for (size_t i = 0; i < header->columns_cnt; ++i) {
      database_column* cur =
          (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE) + i;
      if (!strcmp(((protocol_column*)columns->pdata[j])->name_, cur->name)) {
        positions[size++] = cur->next;
        break;
      }
    }
  }
  int err_code = 0;
  int count = 0;
  off_t cur_row = header->next_data_block;
  while (cur_row != 0) {
    int code = predicate_info == NULL
                   ? 1
                   : validate_row(positions, columns, predicate_info);
    database_row* next_row;
    if ((next_row = malloc(ROW_PADDING_SIZE)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on allocating memory", NULL);
      err_code = -1;
      goto end;
    }
    if ((err_code = read_from_pos(cur_row, next_row, ROW_PADDING_SIZE)) == -1) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on reading meta-data of a row.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on reading data", NULL);
      free(next_row);
      goto end;
    }
    off_t next_position = next_row->next;
    free(next_row);
    off_t* next_rows;
    if ((next_rows = malloc(sizeof(off_t) * size)) == NULL) {
      logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
             "Error on allocating memory for meta-data of a row.");
      g_object_set(response, "status_", 400, "message_",
                   "Error on allocating memory", NULL);
      err_code = -1;
      goto end;
    }
    for (size_t i = 0; i < size; ++i) {
      database_element* data;
      if ((data = read_element(positions[i], 0)) == NULL) {
        logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
               "Error on reading a meta-data of an element.");
        err_code = -1;
        goto end;
      }
      next_rows[i] = data->next;
    }
    if (code == 1) {
      database_row* row = read_row(cur_row);
      if (row == NULL) {
        err_code = -1;
        g_object_set(response, "status_", 400, "message_",
                     "Error on reading data", NULL);
        goto end;
      }
      if (erase_database_index(cur_row) == -1) {
        err_code = -1;
        g_object_set(response, "status_", 400, "message_",
                     "Error on erasing data", NULL);
        goto end;
      } else {
        ++count;
      }
      protocol_insert_request* req =
          g_object_new(TYPE_PROTOCOL_INSERT_REQUEST, "list_", g_ptr_array_new(),
                       "column_", g_ptr_array_new(), NULL);
      for (size_t i = 0; i < header->columns_cnt; ++i) {
        g_ptr_array_add(
            req->column_,
            g_object_new(
                TYPE_PROTOCOL_COLUMN, "name_",
                ((database_column*)((char*)header + FILE_HEADER_PADDING_SIZE) +
                 i)
                    ->name,
                NULL));
      }
      char* data_start = (char*)row + ROW_PADDING_SIZE;
      database_column* cur_column =
          (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
      for (size_t i = 0; i < header->columns_cnt; ++cur_column, ++i) {
        int flag = 0;
        for (size_t j = 0; j < columns_update->len; ++j) {
          if (!strcmp(((protocol_columns_update*)columns_update->pdata[j])
                          ->column_->name_,
                      cur_column->name)) {
            flag = 1;
            g_ptr_array_add(
                req->list_,
                ((protocol_columns_update*)columns_update->pdata[j])->literal_);
            break;
          }
        }
        database_element* cur = (database_element*)data_start;
        if (flag) {
          data_start += padding(cur->size) + ELEMENT_PADDING_SIZE;
          continue;
        }
        protocol_literal* lit = g_object_new(
            TYPE_PROTOCOL_LITERAL, "data_",
            g_object_new(TYPE_PROTOCOL_LITERAL_UNION_STORAGE, NULL), NULL);
        char* data = (char*)cur + ELEMENT_PADDING_SIZE;
        size_t size1 = 0;
        switch (cur->type) {
          case COLUMN_BOOL:
            g_object_set(lit->data_, "boolean_", *data, NULL);
            break;
          case COLUMN_INT32:
            g_object_set(lit->data_, "integer_", *(int32_t*)data, NULL);
            break;
          case COLUMN_DOUBLE:
            g_object_set(lit->data_, "float_", *(double*)data, NULL);
            break;
          case COLUMN_STRING:
            g_object_set(lit->data_, "string_", data, NULL);
            break;
        }
        g_object_set(lit, "type_", cur->type, NULL);
        g_ptr_array_add(req->list_, lit);
        data_start += padding(cur->size) + ELEMENT_PADDING_SIZE;
      }
      process_insert(req, NULL, NULL);
      g_ptr_array_unref(req->list_);
      g_ptr_array_unref(req->column_);
      g_object_unref(req);
      free(row);
    }
    free(positions);
    positions = next_rows;
    cur_row = next_position;
  }
end:
  if (err_code != -1) {
    char message[20];
    sprintf(message, "UPDATE: %d.", count);
    printf("%d", count);
    g_object_set(response, "status_", 200, "message_", message, NULL);
  }
  g_ptr_array_unref(columns);
  free(positions);
}

void process_request(const protocol_request* req, protocol_response* response) {
  switch (req->type_) {
    case 258:
      process_create(req->data_->create_, req->name_, response);
      break;
    case 261: {
      int err_code = delete_database(req->name_);
      switch (err_code) {
        case -2:
          g_object_set(response, "status_", 404, "message_",
                       "Table does not exist.", NULL);
          break;
        case -1:
          g_object_set(response, "status_", 400, "message_",
                       "Error on deleting database.", NULL);
          break;
        case 0:
          g_object_set(response, "status_", 200, "message_", "DROP: OK.", NULL);
          break;
        default:
          break;
      }
      break;
    }
    case 262:
      process_insert(req->data_->insert_, req->name_, response);
      break;
    case 259:
      process_select(req->data_->select_, req->name_, response);
      break;
    case 263:
      if (req->data_->__isset_delete_)
        process_delete(req->data_->delete_, response);
      else
        process_delete(NULL, response);
      break;
    case 260:
      process_update(req->data_->update_->columns_,
                     req->data_->update_->predicate_, response);
      break;
  }
}
