#ifndef DATABASELLP_DATABASE_H
#define DATABASELLP_DATABASE_H

#include <stdio.h>
#include <sys/types.h>

#include "../../../protocols/gen-c_glib/protocols_types.h"
#include "../../header_utils/include/header_utils.h"
#include "database_utils.h"

int open_database(const char* filename);

int close_database();

void process_request(const protocol_request*, protocol_response* response);

#endif  // DATABASELLP_DATABASE_H
