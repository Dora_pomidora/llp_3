#ifndef DATABASELLP_HEADER_UTILS_H
#define DATABASELLP_HEADER_UTILS_H

#include <stdarg.h>
#include <sys/types.h>

#include "../../../protocols/gen-c_glib/protocols_types.h"
#include "database_utils.h"

#define NAME_LENGTH 256

typedef struct {
  size_t size;
  off_t prev;
  off_t next;
  char type;
  char data[];
} database_element;

typedef struct {
  off_t prev;
  off_t next;
  char type;
  char name[NAME_LENGTH];
} database_column;

typedef struct {
  size_t size;
  size_t padding;
  off_t prev;
  off_t next;
  char data[];
} database_row;

typedef struct {
  size_t size;
  char name[NAME_LENGTH];
  size_t columns_cnt;
  off_t prev_data_block;
  off_t next_data_block;
  off_t prev_free_block;
  off_t next_free_block;
  char data[];
} database_file_header;

size_t padding(size_t size);

database_file_header* create_header(const char* filename,
                                    const GPtrArray* columns);

database_file_header* read_file_header();

int write_file_header(const database_file_header* header);

database_row* create_row(const database_file_header* header,
                         protocol_insert_request* req);

database_row* read_row(off_t pos);

int write_row(off_t pos, const database_row* data);

database_element* read_element(off_t pos, char type);

int write_element(off_t pos, const database_element* data, char type);

#define ROW_PADDING_SIZE (padding(sizeof(database_row)))

#define ELEMENT_PADDING_SIZE (padding(sizeof(database_element)))

#define FILE_HEADER_PADDING_SIZE (padding(sizeof(database_file_header)))

#define COLUMN_PADDING_SIZE (padding(sizeof(database_column)))

#endif  // DATABASELLP_HEADER_UTILS_H
