#include "header_utils.h"

#include <memory.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "../../../parse/lex.yy.h"
#include "../../../parse/parse_request.h"
#include "../../../parse/sql_parse.tab.h"
#include "file_interface.h"
#include "logger.h"

size_t padding(size_t size) {
  if (size % CHUNK_SIZE == 0) {
    return size;
  }
  return size - (size % CHUNK_SIZE) + CHUNK_SIZE;
}

database_file_header* create_header(const char* filename,
                                    const GPtrArray* columns) {
  size_t size = FILE_HEADER_PADDING_SIZE + columns->len * COLUMN_PADDING_SIZE;
  database_file_header* header;
  if ((header = malloc(size)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a file header.");
    return NULL;
  }
  memset(header, 0, size);
  header->size = size;
  header->columns_cnt = columns->len;
  strcpy(header->name, filename);
  database_column* cur_column =
      (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < columns->len; ++i, ++cur_column) {
    const protocol_columns_definition* cur_column_def =
        (const protocol_columns_definition*)columns->pdata[i];
    switch (cur_column_def->type_) {
      case BOOLEAN:
        cur_column->type = COLUMN_BOOL;
        break;
      case INTEGER:
        cur_column->type = COLUMN_INT32;
        break;
      case FLOAT:
        cur_column->type = COLUMN_DOUBLE;
        break;
      case STRING:
        cur_column->type = COLUMN_STRING;
        break;
    }
    strcpy(cur_column->name, cur_column_def->name_);
  }
  return header;
}

database_file_header* read_file_header() {
  database_file_header* header;
  if ((header = malloc(FILE_HEADER_PADDING_SIZE)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for meta-data of file header.");
    return NULL;
  }
  if (read_from_pos(0, header, FILE_HEADER_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of file-header.");
    free(header);
    return NULL;
  }
  size_t size = header->size;
  free(header);
  if ((header = malloc(size)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for file-header.");
    return NULL;
  }
  if (read_from_pos(0, header, size) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading file-header.");
    free(header);
    return NULL;
  }
  return header;
}

int write_file_header(const database_file_header* header) {
  if (write_in_pos(0, header, header->size) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing file-header.");
    return -1;
  }
  return 0;
}

size_t calculate_row_size(const database_file_header* header,
                          protocol_insert_request* req) {
  size_t size = ROW_PADDING_SIZE;
  database_column* cur_column =
      (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    size_t element_size = ELEMENT_PADDING_SIZE;
    switch (cur_column->type) {
      case COLUMN_STRING: {
        element_size +=
            strlen(((protocol_literal*)req->list_->pdata[i])->data_->string_) +
            1;
        break;
      }
      case COLUMN_DOUBLE: {
        element_size += sizeof(double);
        break;
      }
      case COLUMN_INT32: {
        element_size += sizeof(int32_t);
        break;
      }
      case COLUMN_BOOL: {
        element_size += sizeof(char);
        break;
      }
    }
    size += padding(element_size);
  }
  return size;
}

database_row* create_row(const database_file_header* header,
                         protocol_insert_request* req) {
  size_t size = calculate_row_size(header, req);
  database_row* data;
  if ((data = malloc(size)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a database_row.");
    return NULL;
  }
  memset(data, 0, size);
  data->size = size;
  database_column* cur_column =
      (database_column*)((char*)header + FILE_HEADER_PADDING_SIZE);
  char* cur_data = (char*)data + ROW_PADDING_SIZE;
  for (size_t i = 0; i < header->columns_cnt; ++i, ++cur_column) {
    database_element* cur_element = (database_element*)cur_data;
    char* data_start = cur_data + ELEMENT_PADDING_SIZE;
    cur_element->type = cur_column->type;
    switch (cur_column->type) {
      case COLUMN_STRING: {
        cur_element->size =
            strlen(((protocol_literal*)req->list_->pdata[i])->data_->string_) +
            1;
        memcpy(data_start,
               ((protocol_literal*)req->list_->pdata[i])->data_->string_,
               cur_element->size);
        break;
      }
      case COLUMN_DOUBLE: {
        cur_element->size = sizeof(double);
        memcpy(data_start,
               &((protocol_literal*)req->list_->pdata[i])->data_->float_,
               cur_element->size);
        break;
      }
      case COLUMN_INT32: {
        cur_element->size = sizeof(int32_t);
        memcpy(data_start,
               &((protocol_literal*)req->list_->pdata[i])->data_->integer_,
               cur_element->size);
        break;
      }
      case COLUMN_BOOL: {
        cur_element->size = sizeof(char);
        char bool = ((protocol_literal*)req->list_->pdata[i])->data_->boolean_;
        memcpy(data_start, &bool, cur_element->size);
        break;
      }
    }
    cur_data += ELEMENT_PADDING_SIZE + padding(cur_element->size);
  }
  return data;
}

database_row* read_row(off_t pos) {
  database_row* data;
  if ((data = malloc(ROW_PADDING_SIZE)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a database_row meta-data.");
    return NULL;
  }
  if (read_from_pos(pos, data, ROW_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of a database_row.");
    free(data);
    return NULL;
  }
  size_t size = data->size + data->padding;
  free(data);
  if ((data = malloc(size)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for a database_row.");
    return NULL;
  }
  if (read_from_pos(pos, data, size) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading data of a database_row.");
    free(data);
    return NULL;
  }
  return data;
}

int write_row(off_t pos, const database_row* data) {
  if (write_in_pos(pos, data, data->size + data->padding) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing an a database_row.");
    return -1;
  }
  return 0;
}

database_element* read_element(off_t pos, char type) {
  database_element* data;
  if ((data = malloc(ELEMENT_PADDING_SIZE)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for meta-data of an "
           "database_element.");
    return NULL;
  }
  if (read_from_pos(pos, data, ELEMENT_PADDING_SIZE) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading meta-data of an database_element.");
    free(data);
    return NULL;
  }
  if (!type) {
    return data;
  }
  size_t size = ELEMENT_PADDING_SIZE + padding(data->size);
  free(data);
  if ((data = malloc(size)) == NULL) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on allocating memory for an database_element.");
    return NULL;
  }
  if (read_from_pos(pos, data, size) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on reading an database_element.");
    free(data);
    return NULL;
  }
  return data;
}

int write_element(off_t pos, const database_element* data, char type) {
  size_t size = ELEMENT_PADDING_SIZE;
  if (type) {
    size += padding(data->size);
  }
  if (write_in_pos(pos, data, size) == -1) {
    logger(LOGGER_ERROR, __FILE__, __func__, __LINE__,
           "Error on writing an database_element.");
    return -1;
  }
  return 0;
}
