#ifndef DALLVOROLLP2_STRUCTURES_H
#define DALLVOROLLP2_STRUCTURES_H

typedef enum literal_t literal_t;
typedef enum predicate_type predicate_type;
typedef enum compare_right_type compare_right_type;
typedef struct binary binary;
typedef struct compare compare;
typedef struct column column;
typedef struct compare_right_part compare_right_part;
typedef struct select_request select_request;
typedef struct insert_request insert_request;
typedef struct update_request update_request;
typedef struct request request;
typedef struct literal literal;
typedef struct predicate predicate;
typedef struct literal_list literal_list;
typedef struct columns_definition columns_definition;
typedef struct columns_update columns_update;

enum literal_t {
  LITERAL_BOOLEAN,
  LITERAL_INTEGER,
  LITERAL_FLOAT,
  LITERAL_STRING,
};

struct literal {
  literal_t _type;
  union {
    int _boolean;
    int _integer;
    float _float;
    char* _string;
  } _data;
};

struct literal_list {
  literal* _literal;
  literal_list* _next;
};

struct column {
  char* _name;
  column* _next;
};

enum predicate_type {
  PREDICATE_COMPARE,
  PREDICATE_BINARY,
  PREDICATE_NEGATIVE,
};

enum compare_right_type {
  COMPARE_LITERAL,
  COMPARE_COLUMN,
};

struct compare_right_part {
  compare_right_type _type;
  union {
    literal* _literal;
    column* _column;
  } value;
};

struct binary {
  predicate* _left;
  predicate* _right;
  int _type;
};

struct compare {
  column* _left;
  compare_right_part* _right;
  int _compare_type;
};

struct predicate {
  union {
    compare* _compare;
    binary* _binary;
    predicate* _negative;
  } content;
  predicate_type _type;
};

struct select_request {
  column* _column;
  predicate* _predicate;
};

struct insert_request {
  column* _column;
  literal_list* _list;
};

struct columns_update {
  column* _column;
  literal* _literal;
  columns_update* _next;
};

struct update_request {
  columns_update* _columns;
  predicate* _predicate;
};

struct request {
  union {
    select_request* _select;
    insert_request* _insert;
    update_request* _update;
    columns_definition* _create;
    predicate* _delete;
  } _data;
  char* _name;
  int _type;
};

struct columns_definition {
  char* _name;
  int _type;
  columns_definition* _next;
};

void deleteRequest(request* data);

column* createColumn(char* name);
column* mergeColumns(column* left, column* right);

literal* createInteger(int value);
literal* createFloat(float value);
literal* createBoolean(int value);
literal* createString(char* value);

literal_list* createLiteralList(literal* data);
literal_list* addLiteralList(literal* left, literal_list* right);

columns_definition* createColumnsDefinition(char* name, int type);
columns_definition* addColumnsDefinition(columns_definition* left,
                                         columns_definition* right);

columns_update* createColumnsUpdate(column* col, literal* lit);
columns_update* addColumnsUpdate(columns_update* left, columns_update* right);

int negative_compare(int compare);

predicate* createLiteralPredicate(column* col, int type, literal* value);
predicate* createColumnsPredicate(column* left, int type, column* right);
predicate* createContainsPredicate(column* col, literal* value);
predicate* createBinary(predicate* left, int type, predicate* right);
predicate* createNegate(predicate* data);

request* createSelect(column* column, char* name);
request* addPredicate(request* request_info, predicate* predicate_info);

request* createInsert(column* column, char* name, literal_list* list);

request* createDrop(char* name);

request* createCreate(columns_definition* columns, char* name);

request* createDelete(char* name, predicate* predicate_info);

request* createUpdate(columns_update* columns, char* name,
                      predicate* predicate_info);

void printRequest(request* data);

#endif  // DALLVOROLLP2_STRUCTURES_H
