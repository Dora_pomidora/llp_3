#ifndef DALLVOROLLP3_PARSE_REQUEST_H
#define DALLVOROLLP3_PARSE_REQUEST_H

#include "structures.h"

struct result_parse {
  int exit_code;
  request* data;
};

struct result_parse parse(char* str);

#endif  // DALLVOROLLP3_PARSE_REQUEST_H
