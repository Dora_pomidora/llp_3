%{
#include <stdio.h>
#include <stdlib.h>
#include "structures.h"
extern int yylex();
extern int yylineno;
#define YYERROR_VERBOSE 1
void yyerror(request** request, const char* str) {
    fprintf(stderr, "%s:%d error", str, yylineno);
}
%}

%token CREATE SELECT UPDATE DROP INSERT DELETE
%token FROM TABLE WHERE INTO VALUES SET
%token TYPE PREDICATE
%token INTEGER STRING FLOAT BOOLEAN
%token SEMICOLON COMA QUOTE OPENING_PARENTHESIS CLOSING_PARENTHESIS
%token OR AND NOT NO_OP CONTAINS IS NOT_EQUAL LESS LESS_EQUAL GREATER GREATER_EQUAL COMPARE EQUAL
%token ERROR NONE QUIT

%type <_string> STRING
%type <_string> tablename
%type <_boolean> BOOLEAN
%type <_integer> INTEGER
%type <_float> FLOAT
%type <_type> TYPE
%type <_column> column
%type <_column> simple_column
%type <_literal> literal
%type <_literal> string_literal
%type <_request> request
%type <_request> select_request
%type <_request> simple_select_request
%type <_request> create_request
%type <_request> update_request
%type <_request> drop_request
%type <_request> insert_request
%type <_request> delete_request
%type <_predicate> predicate
%type <_predicate> simple_predicate
%type <_compare> COMPARE
%type <_literal_list> values_list
%type <_columns_definition> columns_create
%type <_columns_definition> simple_columns_create
%type <_columns_update> columns_update
%type <_columns_update> simple_columns_update

%left OR
%left AND

%union {
    int _boolean;
    int _integer;
    float _float;
    char _string[256];
    int _type;
    int _compare;
    literal* _literal;
    column* _column;
    request* _request;
    predicate* _predicate;
    literal_list* _literal_list;
    columns_definition* _columns_definition;
    columns_update* _columns_update;
}

%parse-param { request** rq }

%start input

%%
input: | input request SEMICOLON { *rq = $2; };
request: QUIT { return 2; } | select_request | create_request | update_request | drop_request | insert_request | delete_request;

select_request: simple_select_request | simple_select_request WHERE predicate { $$ = addPredicate($1, $3); };
simple_select_request: SELECT column FROM tablename { $$ = createSelect($2, $4); }

column: simple_column COMA column { $$ = mergeColumns($1, $3); } | simple_column;
simple_column: STRING { $$ = createColumn($1); };

insert_request: INSERT INTO tablename OPENING_PARENTHESIS column CLOSING_PARENTHESIS VALUES OPENING_PARENTHESIS values_list CLOSING_PARENTHESIS { $$ = createInsert($5, $3, $9); };

drop_request: DROP TABLE tablename { $$ = createDrop($3); };

create_request: CREATE TABLE tablename OPENING_PARENTHESIS columns_create CLOSING_PARENTHESIS { $$ = createCreate($5, $3); };

delete_request: DELETE FROM tablename WHERE predicate { $$ = createDelete($3, $5); };

update_request: UPDATE tablename SET columns_update WHERE predicate { $$ = createUpdate($4, $2, $6); };

tablename: STRING;

values_list: literal { $$ = createLiteralList($1); } | literal COMA values_list { $$ = addLiteralList($1, $3); };

columns_create: simple_columns_create | simple_columns_create COMA columns_create { $$ = addColumnsDefinition($1, $3); };
simple_columns_create: STRING TYPE { $$ = createColumnsDefinition($1, $2); };

columns_update: simple_columns_update | simple_columns_update COMA columns_update { $$ = addColumnsUpdate($1, $3); };
simple_columns_update: simple_column EQUAL literal { $$ = createColumnsUpdate($1, $3); };

literal: string_literal
|        BOOLEAN { $$ = createBoolean($1); }
|        INTEGER { $$ = createInteger($1); }
|        FLOAT { $$ = createFloat($1); };
string_literal: QUOTE STRING QUOTE { $$ = createString($2); };

predicate: predicate AND predicate { $$ = createBinary($1, AND, $3); }
|          predicate OR predicate { $$ = createBinary($1, OR, $3); }
|          NOT OPENING_PARENTHESIS predicate CLOSING_PARENTHESIS { $$ = createNegate($3); }
|          simple_predicate { $$ = $1; }
|          OPENING_PARENTHESIS predicate CLOSING_PARENTHESIS { $$ = $2; };

simple_predicate: simple_column COMPARE literal { $$ = createLiteralPredicate($1, $2, $3); }
|                 literal COMPARE simple_column { $$ = createLiteralPredicate($3, negative_compare($2), $1); }
|                 simple_column COMPARE simple_column { $$ = createColumnsPredicate($1, $2, $3); }
|                 CONTAINS OPENING_PARENTHESIS simple_column COMA string_literal CLOSING_PARENTHESIS { $$ = createContainsPredicate($3, $5); };
%%

