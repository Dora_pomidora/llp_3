#include "structures.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sql_parse.tab.h"

static char* compare_to_str[] = {[CONTAINS] = "CONTAINS", [IS] = "==",
                                 [NOT_EQUAL] = "!=",      [LESS] = "<",
                                 [LESS_EQUAL] = "<=",     [GREATER] = ">",
                                 [GREATER_EQUAL] = "=>"};

void printWithTabulation(size_t tabs, char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  for (size_t i = 0; i < tabs; ++i) {
    printf("\t");
  }
  vprintf(fmt, args);
  va_end(args);
}

void printAllColumns(size_t tabs, column* data) {
  printWithTabulation(tabs, "\"%s\"", data->_name);
  if (data->_next != NULL) {
    printf(",");
  }
  printf("\n");
  if (data->_next != NULL) {
    printAllColumns(tabs, data->_next);
  }
}

void printColumns(size_t tabs, column* data) {
  if (data == NULL) {
    return;
  }
  printf(",\n");
  printWithTabulation(tabs, "\"columns\": [\n");
  printAllColumns(tabs + 1, data);
  printWithTabulation(tabs, "]");
}

void printLiteral(literal* data) {
  switch (data->_type) {
    case LITERAL_BOOLEAN:
      printf("%s", (data->_data._boolean ? "true" : "false"));
      break;
    case LITERAL_INTEGER:
      printf("%d", data->_data._integer);
      break;
    case LITERAL_FLOAT:
      printf("%f", data->_data._float);
      break;
    case LITERAL_STRING:
      printf("\"%s\"", data->_data._string);
      break;
  }
}

void printRightPart(size_t tabs, compare_right_part* data) {
  printWithTabulation(tabs++, "\"right_part\": {\n");
  static char* right_part_to_str[] = {
      [COMPARE_COLUMN] = "column", [COMPARE_LITERAL] = "literal"};
  printWithTabulation(tabs, "\"type\": \"%s\",\n",
                      right_part_to_str[data->_type]);
  printWithTabulation(tabs, "\"value\": ");
  if (data->_type == COMPARE_COLUMN) {
    printf("\"%s\"\n", data->value._column->_name);
  } else {
    printLiteral(data->value._literal);
    printf("\n");
  }
  printWithTabulation(--tabs, "}");
}

void printAllPredicate(size_t tabs, predicate* data) {
  static char* binary_to_str[] = {[AND] = "AND", [OR] = "OR"};
  switch (data->_type) {
    case PREDICATE_NEGATIVE:
      printWithTabulation(tabs, "\"type\": \"NOT\",\n");
      printWithTabulation(tabs, "\"predicate\": {\n");
      printAllPredicate(tabs + 1, data->content._negative);
      printf("\n");
      printWithTabulation(tabs, "}");
      break;
    case PREDICATE_COMPARE:
      printWithTabulation(
          tabs, "\"type\": \"%s\",\n",
          compare_to_str[data->content._compare->_compare_type]);
      printWithTabulation(tabs, "\"column_name\": \"%s\",\n",
                          data->content._compare->_left->_name);
      printRightPart(tabs, data->content._compare->_right);
      break;
    case PREDICATE_BINARY:
      printWithTabulation(tabs, "\"type\": \"BINARY\",\n");
      printWithTabulation(tabs, "\"operand\": \"%s\",\n",
                          binary_to_str[data->content._binary->_type]);
      printWithTabulation(tabs, "\"left\": {\n");
      printAllPredicate(tabs + 1, data->content._binary->_left);
      printf("\n");
      printWithTabulation(tabs, "},\n");
      printWithTabulation(tabs, "\"right\": {\n");
      printAllPredicate(tabs + 1, data->content._binary->_right);
      printf("\n");
      printWithTabulation(tabs, "}");
  }
}

void printAllLiteralList(size_t tabs, literal_list* data) {
  printWithTabulation(tabs, "");
  printLiteral(data->_literal);
  if (data->_next != NULL) {
    printf(",");
  }
  printf("\n");
  if (data->_next != NULL) {
    printAllLiteralList(tabs, data->_next);
  }
}

void printLiteralList(size_t tabs, literal_list* data) {
  if (data == NULL) {
    return;
  }
  printf(",\n");
  printWithTabulation(tabs, "\"values\": [\n");
  printAllLiteralList(tabs + 1, data);
  printWithTabulation(tabs, "]");
}

void printPredicate(size_t tabs, predicate* data) {
  if (data == NULL) {
    return;
  }
  printf(",\n");
  printWithTabulation(tabs, "\"predicate\": {\n");
  printAllPredicate(tabs + 1, data);
  printf("\n");
  printWithTabulation(tabs, "}");
}

void printAllColumnsUpdate(size_t tabs, columns_update* data) {
  printWithTabulation(tabs++, "{\n");
  printWithTabulation(tabs, "\"column\": \"%s\",\n", data->_column->_name);
  printWithTabulation(tabs, "\"value\": ");
  printLiteral(data->_literal);
  printf("\n");
  printWithTabulation(--tabs, "}");
  if (data->_next != NULL) {
    printf(",");
  }
  printf("\n");
  if (data->_next != NULL) {
    printAllColumnsUpdate(tabs, data->_next);
  }
}

void printColumnsUpdate(size_t tabs, columns_update* data) {
  if (data == NULL) {
    return;
  }
  printf(",\n");
  printWithTabulation(tabs, "\"update\": [\n");
  printAllColumnsUpdate(tabs + 1, data);
  printf("\n");
  printWithTabulation(tabs, "]");
}

void deleteColumns(column* data) {
  while (data != NULL) {
    column* next = data->_next;
    free(data->_name);
    free(data);
    data = next;
  }
}

void deleteLiteral(literal* data) {
  if (data == NULL) {
    return;
  }
  if (data->_type == LITERAL_STRING) {
    free(data->_data._string);
  }
  free(data);
}

void deleteLiteralList(literal_list* list) {
  while (list != NULL) {
    literal_list* next = list->_next;
    deleteLiteral(list->_literal);
    free(list);
    list = next;
  }
}

void deleteColumnsDefinition(columns_definition* columns) {
  while (columns != NULL) {
    columns_definition* next = columns->_next;
    free(columns->_name);
    free(columns);
    columns = next;
  }
}

void deleteColumnsUpdate(columns_update* columns) {
  while (columns != NULL) {
    columns_update* next = columns->_next;
    deleteColumns(columns->_column);
    deleteLiteral(columns->_literal);
    free(columns);
    columns = next;
  }
}

void deleteCompare(compare_right_part* data) {
  if (data == NULL) {
    return;
  }
  switch (data->_type) {
    case COMPARE_LITERAL:
      deleteLiteral(data->value._literal);
      break;
    case COMPARE_COLUMN:
      deleteColumns(data->value._column);
      break;
  }
  free(data);
}

void deletePredicate(predicate* data) {
  if (data == NULL) {
    return;
  }
  switch (data->_type) {
    case PREDICATE_COMPARE:
      deleteColumns(data->content._compare->_left);
      deleteCompare(data->content._compare->_right);
      free(data->content._compare);
      break;
    case PREDICATE_BINARY:
      deletePredicate(data->content._binary->_left);
      deletePredicate(data->content._binary->_right);
      free(data->content._binary);
      break;
    case PREDICATE_NEGATIVE:
      deletePredicate(data->content._negative);
      break;
  }
  free(data);
}

void deleteSelect(select_request* data) {
  if (data == NULL) {
    return;
  }
  deleteColumns(data->_column);
  deletePredicate(data->_predicate);
  free(data);
}

void deleteInsert(insert_request* data) {
  if (data == NULL) {
    return;
  }
  deleteColumns(data->_column);
  deleteLiteralList(data->_list);
  free(data);
}

void deleteUpdate(update_request* data) {
  if (data == NULL) {
    return;
  }
  deletePredicate(data->_predicate);
  deleteColumnsUpdate(data->_columns);
  free(data);
}

void deleteRequest(request* data) {
  if (data == NULL) {
    return;
  }
  free(data->_name);
  switch (data->_type) {
    case SELECT:
      deleteSelect(data->_data._select);
      break;
    case CREATE:
      deleteColumnsDefinition(data->_data._create);
      break;
    case DROP:
      break;
    case INSERT:
      deleteInsert(data->_data._insert);
      break;
    case DELETE:
      deletePredicate(data->_data._delete);
      break;
    case UPDATE:
      deleteUpdate(data->_data._update);
      break;
  }
  free(data);
}

column* createColumn(char* name) {
  column* data = malloc(sizeof(column));
  if (data == NULL) {
    return NULL;
  }
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    free(data);
    return NULL;
  }
  data->_next = NULL;
  data->_name[strlen(name)] = '\0';
  return data;
}

column* mergeColumns(column* left, column* right) {
  left->_next = right;
  return left;
}

literal* createInteger(int value) {
  literal* data = malloc(sizeof(literal));
  if (data == NULL) {
    return NULL;
  }
  data->_type = LITERAL_INTEGER;
  data->_data._integer = value;
  return data;
}

literal* createFloat(float value) {
  literal* data = malloc(sizeof(literal));
  if (data == NULL) {
    return NULL;
  }
  data->_type = LITERAL_FLOAT;
  data->_data._float = value;
  return data;
}

literal* createBoolean(int value) {
  literal* data = malloc(sizeof(literal));
  if (data == NULL) {
    return NULL;
  }
  data->_type = LITERAL_BOOLEAN;
  data->_data._boolean = value;
  return data;
}

literal* createString(char* value) {
  literal* data = malloc(sizeof(literal));
  if (data == NULL) {
    return NULL;
  }
  if ((data->_data._string = strcpy(malloc(strlen(value) + 1), value)) ==
      NULL) {
    free(data);
    return NULL;
  }
  data->_data._string[strlen(value)] = '\0';
  data->_type = LITERAL_STRING;
  return data;
}

literal_list* createLiteralList(literal* data) {
  literal_list* list = malloc(sizeof(literal_list));
  if (list == NULL) {
    deleteLiteral(data);
    return NULL;
  }
  list->_literal = data;
  list->_next = NULL;
  return list;
}

literal_list* addLiteralList(literal* left, literal_list* right) {
  literal_list* newHead = createLiteralList(left);
  if (newHead == NULL) {
    deleteLiteralList(right);
    deleteLiteral(left);
    return NULL;
  }
  newHead->_next = right;
  return newHead;
}

columns_definition* createColumnsDefinition(char* name, int type) {
  columns_definition* data = malloc(sizeof(columns_definition));
  if (data == NULL) {
    return NULL;
  }
  data->_next = NULL;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  data->_type = type;
  return data;
}

columns_definition* addColumnsDefinition(columns_definition* left,
                                         columns_definition* right) {
  left->_next = right;
  return left;
}

columns_update* createColumnsUpdate(column* col, literal* lit) {
  columns_update* data = malloc(sizeof(columns_update));
  if (data == NULL) {
    deleteColumns(col);
    deleteLiteral(lit);
    return NULL;
  }
  data->_next = NULL;
  data->_column = col;
  data->_literal = lit;
  return data;
}

columns_update* addColumnsUpdate(columns_update* left, columns_update* right) {
  left->_next = right;
  return left;
}

int negative_compare(int compare) {
  switch (compare) {
    case IS:
      return IS;
    case NOT_EQUAL:
      return NOT_EQUAL;
    case LESS:
      return GREATER;
    case LESS_EQUAL:
      return GREATER_EQUAL;
    case GREATER:
      return LESS;
    case GREATER_EQUAL:
      return LESS_EQUAL;
  }
  return -1;
}

predicate* createLiteralPredicate(column* col, int type, literal* value) {
  predicate* data = malloc(sizeof(predicate));
  if (data == NULL) {
    deleteColumns(col);
    deleteLiteral(value);
    return NULL;
  }
  data->_type = PREDICATE_COMPARE;
  if ((data->content._compare = malloc(sizeof(compare))) == NULL) {
    free(data);
    deleteColumns(col);
    deleteLiteral(value);
    return NULL;
  }
  if ((data->content._compare->_right = malloc(sizeof(compare_right_part))) ==
      NULL) {
    free(data->content._compare);
    free(data);
    deleteColumns(col);
    deleteLiteral(value);
    return NULL;
  }
  data->content._compare->_left = col;
  data->content._compare->_compare_type = type;
  data->content._compare->_right->_type = COMPARE_LITERAL;
  data->content._compare->_right->value._literal = value;
  return data;
}

predicate* createColumnsPredicate(column* left, int type, column* right) {
  predicate* data = malloc(sizeof(predicate));
  if (data == NULL) {
    deleteColumns(left);
    deleteColumns(right);
    return NULL;
  }
  data->_type = PREDICATE_COMPARE;
  if ((data->content._compare = malloc(sizeof(compare))) == NULL) {
    free(data);
    deleteColumns(left);
    deleteColumns(right);
    return NULL;
  }
  if ((data->content._compare->_right = malloc(sizeof(compare_right_part))) ==
      NULL) {
    free(data->content._compare);
    free(data);
    deleteColumns(left);
    deleteColumns(right);
    return NULL;
  }
  data->content._compare->_left = left;
  data->content._compare->_compare_type = type;
  data->content._compare->_right->_type = COMPARE_COLUMN;
  data->content._compare->_right->value._column = right;
  return data;
}

predicate* createContainsPredicate(column* col, literal* value) {
  return createLiteralPredicate(col, CONTAINS, value);
}

predicate* createBinary(predicate* left, int type, predicate* right) {
  predicate* data = malloc(sizeof(predicate));
  if (data == NULL) {
    deletePredicate(left);
    deletePredicate(right);
    return NULL;
  }
  data->_type = PREDICATE_BINARY;
  if ((data->content._binary = malloc(sizeof(binary))) == NULL) {
    free(data);
    deletePredicate(left);
    deletePredicate(right);
    return NULL;
  }
  data->content._binary->_left = left;
  data->content._binary->_type = type;
  data->content._binary->_right = right;
  return data;
}

predicate* createNegate(predicate* data) {
  predicate* result = malloc(sizeof(predicate));
  if (result == NULL) {
    deletePredicate(data);
    return NULL;
  }
  result->_type = PREDICATE_NEGATIVE;
  result->content._negative = data;
  return result;
}

request* createSelect(column* column, char* name) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    deleteColumns(column);
    return NULL;
  }
  data->_type = SELECT;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    deleteColumns(column);
    free(data);
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  if ((data->_data._select = malloc(sizeof(select_request))) == NULL) {
    deleteColumns(column);
    free(data->_name);
    free(data);
    return NULL;
  }
  data->_data._select->_column = column;
  data->_data._select->_predicate = NULL;
  return data;
}

request* addPredicate(request* request_info, predicate* predicate_info) {
  switch (request_info->_type) {
    case SELECT:
      request_info->_data._select->_predicate = predicate_info;
      break;
  }
  return request_info;
}

request* createInsert(column* column, char* name, literal_list* list) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    deleteColumns(column);
    deleteLiteralList(list);
    return NULL;
  }
  data->_type = INSERT;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    free(data);
    deleteColumns(column);
    deleteLiteralList(list);
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  if ((data->_data._insert = malloc(sizeof(insert_request))) == NULL) {
    free(data->_name);
    free(data);
    deleteColumns(column);
    deleteLiteralList(list);
    return NULL;
  }
  data->_data._insert->_column = column;
  data->_data._insert->_list = list;
  return data;
}

request* createDrop(char* name) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    return NULL;
  }
  data->_type = DROP;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  return data;
}

request* createCreate(columns_definition* columns, char* name) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    deleteColumnsDefinition(columns);
    return NULL;
  }
  data->_type = CREATE;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    deleteColumnsDefinition(columns);
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  data->_data._create = columns;
  return data;
}

request* createDelete(char* name, predicate* predicate_info) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    deletePredicate(predicate_info);
    return NULL;
  }
  data->_type = DELETE;
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    deletePredicate(predicate_info);
    return NULL;
  }
  data->_name[strlen(name)] = '\0';
  data->_data._delete = predicate_info;
  return data;
}

request* createUpdate(columns_update* columns, char* name,
                      predicate* predicate_info) {
  request* data = malloc(sizeof(request));
  if (data == NULL) {
    deletePredicate(predicate_info);
    deleteColumnsUpdate(columns);
    return NULL;
  }
  if ((data->_data._update = malloc(sizeof(update_request))) == NULL) {
    deletePredicate(predicate_info);
    deleteColumnsUpdate(columns);
    free(data);
    return NULL;
  }
  if ((data->_name = strcpy(malloc(strlen(name) + 1), name)) == NULL) {
    deletePredicate(predicate_info);
    deleteColumnsUpdate(columns);
    free(data->_data._update);
    free(data);
    return NULL;
  }
  data->_type = UPDATE;
  data->_name[strlen(name)] = '\0';
  data->_data._update->_columns = columns;
  data->_data._update->_predicate = predicate_info;
  return data;
}

void printSelect(request* data) {
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"select\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\"", data->_name);
  printColumns(tabsCount, data->_data._select->_column);
  printPredicate(tabsCount, data->_data._select->_predicate);
  printf("\n}");
}

void printInsert(request* data) {
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"insert\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\"", data->_name);
  printColumns(tabsCount, data->_data._insert->_column);
  printLiteralList(tabsCount, data->_data._insert->_list);
  printf("\n}");
}

void printDrop(request* data) {
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"drop\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\"", data->_name);
  printf("\n}");
}

void printCreate(request* data) {
  static char* type_to_string[] = {[STRING] = "STRING",
                                   [BOOLEAN] = "BOOLEAN",
                                   [INTEGER] = "INTEGER",
                                   [FLOAT] = "FLOAT"};
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"create\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\",\n", data->_name);
  printWithTabulation(tabsCount++, "\"definitions\": [");
  if (data->_data._create != NULL) {
    printf("\n");
    columns_definition* columns = data->_data._create;
    while (columns != NULL) {
      printWithTabulation(tabsCount++, "{\n");
      printWithTabulation(tabsCount, "\"name\": \"%s\",\n", columns->_name);
      printWithTabulation(tabsCount, "\"type\": \"%s\"\n",
                          type_to_string[columns->_type]);
      printWithTabulation(--tabsCount, "}");
      if (columns->_next != NULL) {
        printf(",");
      }
      printf("\n");
      columns = columns->_next;
    }
    printWithTabulation(--tabsCount, "]\n");
  } else {
    printf("]\n");
  }
  printf("}");
}

void printDelete(request* data) {
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"delete\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\"", data->_name);
  printPredicate(tabsCount, data->_data._delete);
  printf("\n}");
}

void printUpdate(request* data) {
  printf("{\n");
  size_t tabsCount = 1;
  printWithTabulation(tabsCount, "\"type\": \"update\",\n");
  printWithTabulation(tabsCount, "\"tablename\": \"%s\"", data->_name);
  printPredicate(tabsCount, data->_data._update->_predicate);
  printColumnsUpdate(tabsCount, data->_data._update->_columns);
  printf("\n}");
}

void printRequest(request* data) {
  if (data != NULL) {
    switch (data->_type) {
      case SELECT:
        printSelect(data);
        break;
      case INSERT:
        printInsert(data);
        break;
      case DROP:
        printDrop(data);
        break;
      case CREATE:
        printCreate(data);
        break;
      case DELETE:
        printDelete(data);
        break;
      case UPDATE:
        printUpdate(data);
        break;
    }
  }
  printf("\n");
}
