%option noyywrap
%option yylineno

%{
#include <stdio.h>
#include "structures.h"
#include "sql_parse.tab.h"
#define YYERROR_VERBOSE 1
%}

integer [-+]?([0-9]+)
float   [+-]?([0-9]*[.])?[0-9]+
string  ([a-zA-Z0-9_]*)

%%
"quit"      { return QUIT; }

"create"    { return CREATE; }
"select"    { return SELECT; }
"update"    { return UPDATE; }
"drop"      { return DROP; }
"insert"    { return INSERT; }
"delete"    { return DELETE; }

"from"      { return FROM; }
"table"     { return TABLE; }
"where"     { return WHERE; }
"into"      { return INTO; }
"values"    { return VALUES; }
"set"       { return SET; }

"integer"   { yylval._type = INTEGER; return TYPE; }
"string"    { yylval._type = STRING; return TYPE; }
"float"     { yylval._type = FLOAT; return TYPE; }
"boolean"   { yylval._type = BOOLEAN; return TYPE; }

"true"      { yylval._boolean = 1; return BOOLEAN; }
"false"     { yylval._boolean = 0; return BOOLEAN; }

"and"       { return AND; }
"or"        { return OR; }
"not"       { return NOT; }

"="         { return EQUAL; }

"contains"  { return CONTAINS; }
"=="        { yylval._compare = IS; return COMPARE; }
"!="        { yylval._compare = NOT_EQUAL; return COMPARE; }
"<"         { yylval._compare = LESS; return COMPARE; }
">"         { yylval._compare = GREATER; return COMPARE; }
"<="        { yylval._compare = LESS_EQUAL; return COMPARE; }
">="        { yylval._compare = GREATER_EQUAL; return COMPARE; }

";"         { return SEMICOLON; }
","         { return COMA; }
"'"         { return QUOTE; }
"("         { return OPENING_PARENTHESIS; }
")"         { return CLOSING_PARENTHESIS; }

[ \n\r\t] ;
{integer}   { yylval._integer = atoi(yytext); return INTEGER; }
{float}     { yylval._float = atof(yytext); return FLOAT; }
{string}    { sscanf(yytext, "%s", yylval._string); return STRING; }
.           { return ERROR; }

%%
