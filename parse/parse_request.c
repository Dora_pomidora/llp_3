#include "parse_request.h"

#include "lex.yy.h"
#include "sql_parse.tab.h"

extern int yyparse(request**);

struct result_parse parse(char* str) {
  struct result_parse result = {.exit_code = 0,
                                .data = malloc(sizeof(request))};
  yy_scan_string(str);
  if (yyparse(&result.data) == 2) {
    result.exit_code = 1;
  }
  yylex_destroy();
  return result;
}
