project(parse)

file(GLOB HEADERS_FILES *.h)
file(GLOB SOURCES_FILES *.c)

add_library(${PROJECT_NAME} ${HEADERS_FILES} ${SOURCES_FILES})

target_include_directories(${PROJECT_NAME} PUBLIC include)

add_library(DallvoroLLP3::${PROJECT_NAME} ALIAS ${PROJECT_NAME})