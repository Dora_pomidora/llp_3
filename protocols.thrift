union protocol_literal_union_storage {
    1: i32 boolean_
    2: i32 integer_
    3: double float_
    4: string string_
}

struct protocol_literal {
    1: i32 type_
    2: protocol_literal_union_storage data_
}

struct protocol_column {
    1: string name_
}

union protocol_compare_right_part_union {
    1: protocol_literal literal_
    2: protocol_column column_
}

struct protocol_compare_right_part {
    1: i32 type_
    2: protocol_compare_right_part_union value_
}

struct protocol_binary {
    1: list<protocol_predicate> left_
    2: list<protocol_predicate> right_
    3: i32 type_
}

struct protocol_compare {
    1: protocol_column left_
    2: protocol_compare_right_part right_
    3: i32 compare_type_
}

union protocol_predicate_union {
    1: protocol_compare compare_
    2: protocol_binary binary_
    3: list<protocol_predicate> negative_
}

struct protocol_predicate {
    1: protocol_predicate_union content_
    2: i32 type_
}

struct protocol_select_request {
    1: list<protocol_column> column_
    2: list<protocol_predicate> predicate_
}

struct protocol_insert_request {
    1: list<protocol_column> column_
    2: list<protocol_literal> list_
}

struct protocol_columns_update {
    1: protocol_column column_
    2: protocol_literal literal_
}

struct protocol_update_request {
    1: list<protocol_columns_update> columns_
    2: protocol_predicate predicate_
}

union protocol_request_union {
    1: protocol_select_request select_
    2: protocol_insert_request insert_
    3: protocol_update_request update_
    4: list<protocol_columns_definition> create_
    5: protocol_predicate delete_
}

struct protocol_request {
    1: protocol_request_union data_
    2: string name_
    3: i32 type_
}

struct protocol_columns_definition {
    1: string name_
    2: i32 type_
}

enum protocol_status_of_response {
    STATUS_ERROR = 400
    STATUS_OK = 200
    STATUS_TABLE_NOT_FOUND = 404
}

struct protocol_row_in_table {
    1: list<protocol_literal> values_
}

struct protocol_table_result {
    1: list<protocol_column> column_name_
    2: list<protocol_row_in_table> data_
}

struct protocol_response {
    1: protocol_status_of_response status_
    2: string message_
    3: optional protocol_table_result result_
}

service protocol_get_service {
   protocol_response execute(1: protocol_request request)
}
